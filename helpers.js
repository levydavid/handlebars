Handlebars.registerHelper('navigation', function(items, options) {
    var out ='<ul class="navigation">';
    for(var i = 0, l = items.length; i < l; i++) {
      var item = item[i];
      out = out +  '<li><a href="'+ item.url +'">'+ item.title +'</a></li>';
    }

    return out + "</ul>";
});